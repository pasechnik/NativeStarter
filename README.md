
## Native Starter Pro - Seed v5.2.0

Thanks for purchasing the Native Starter Pro.

We provide you Native Starter Pro along with its Seed Version.

A project that you can install on the fly to get the basic plumping of Native Starter Pro using React Native and NativeBase.

Follow the documentation to install and get started with the development:

-   [Documentation](http://strapmobile.com/docs/native-starter-pro/master/)
-   [Product Page](http://strapmobile.com/native-starter-pro/)
-	[Change Log](http://gitstrap.com/strapmobile/NativeStarterPro-seed/blob/v5.1.0/ChangeLog.md)

Please do check `FAQs` section in docs for queries.

Happy coding!
